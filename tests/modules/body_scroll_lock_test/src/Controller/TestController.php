<?php

namespace Drupal\body_scroll_lock_test\Controller;

use Drupal\Core\Url;

/**
 * Test controller for test page responses.
 */
class TestController {

  /**
   * Test Page.
   *
   * @return string
   *   Render array with link.
   */
  public function testPage() {
    return [
      'link' => [
        '#title' => 'Login',
        '#type' => 'link',
        '#url' => Url::fromRoute('user.login'),
        '#attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'dialog',
        ],
        '#attached' => [
          'library' => [
            'core/jquery',
            'core/drupal.dialog',
            'core/drupal.dialog.ajax',
          ],
        ],
      ],
    ];
  }

}
