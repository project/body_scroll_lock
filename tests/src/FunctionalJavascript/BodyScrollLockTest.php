<?php

namespace Drupal\Tests\body_scroll_lock\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the body scroll lock functionality.
 *
 * @group body_scroll_lock
 */
class BodyScrollLockTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'body_scroll_lock',
    'body_scroll_lock_test',
  ];

  /**
   * Tests that the overflow style is applied correctly to <body>.
   */
  public function testBodyScrollLock() {
    $this->drupalGet('/test-page');

    $page = $this->getSession()->getPage();
    $web_assert = $this->assertSession();

    // Trigger dialog.
    $page->clickLink("Login");
    $web_assert->waitForElementVisible('css', 'div[role="dialog"]');

    $condition = "(typeof jQuery !== 'undefined' && jQuery('body').css('overflow') === 'hidden')";
    $this->assertJsCondition($condition);

    // Close dialog.
    $close_button = $this->getSession()->getPage()->find("css", '.ui-dialog-titlebar-close');
    $close_button->click();

    $condition = "(typeof jQuery !== 'undefined' && jQuery('div[role=\"dialog\"]').length === 0)";
    $this->assertJsCondition($condition);

    $condition = "(typeof jQuery !== 'undefined' && jQuery('body').css('overflow') === 'visible')";
    $this->assertJsCondition($condition);
  }

}
