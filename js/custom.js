(function ($, bodyScrollLock) {
  $(window).on({
    'dialog:beforecreate': function (event, dialog, $element) {
      if (!$element.is('#drupal-off-canvas')) {
        bodyScrollLock.disableBodyScroll($element[0]);
      }
    },
    'dialog:beforeclose': function (event, dialog, $element) {
      if (!$element.is('#drupal-off-canvas')) {
        // CORE bug. Forcing to clear all locks if there's only a single modal exists.
        // @see: https://drupal.org/project/body_scroll_lock/issues/3123157
        if ($('.ui-dialog').length <= 1) {
          bodyScrollLock.clearAllBodyScrollLocks();
        } else {
          bodyScrollLock.enableBodyScroll($element[0]);
        }
      }
    },
  });
})(jQuery, bodyScrollLock);
